# -*- coding: UTF-8 -*-
import re
import string
import csv
import getopt
import sys
import pandas as pd


def main(file_path, dict_path):
    with open(file_path, 'r') as raw_text:
        raw_str = raw_text.read()
        print(":: File imported.")
        # Split the raw file and save the pieces into raw_lst
        reg0 = re.compile("\n\n")
        raw_lst = reg0.split(raw_str)
        print(":: Raw file processed.")
        # Create the Review List
        review_lst = []
        tokenization(raw_lst, review_lst)
        matching(review_lst, dict_path)


# Split the pieces (sole reviews) to tokens and save into token_lst, and then join token_lst to the review_lst.
def tokenization(raw_lst, review_lst):
    for part in raw_lst:
        # remove the punctuations in the string.
        part = part.translate(str.maketrans('', '', string.punctuation))
        tmp_lst = part.split()
        # print(tmp_lst[1])
        token_lst = [token.lower() for token in tmp_lst if len(token) > 0]
        review_lst.append(token_lst)
    print(":: Token list generated.")
    # print(review_lst[1])


def csv2dict(file_path, key, value):
    new_dict = {}
    with open(file_path, 'r', encoding='utf-8-sig') as f:
        reader = csv.DictReader(f, fieldnames=['word', 'score'], delimiter=',')
        for row in reader:
            new_dict[row[key]] = row[value]
        print(":: Dictionary imported.")
    return new_dict


def matching(review_lst, dict_path):
    csv_dict = csv2dict(dict_path, 'word', 'score')
    # df_dict = pd.read_csv(dict_path, header=None, usecols=[0, 1], names=['word', 'score'])
    result_lst = []
    sentiment = 0
    for token_lst in review_lst:
        df_lst = pd.DataFrame(token_lst, columns=['word'])
        df_lst['score'] = pd.to_numeric(df_lst['word'].map(csv_dict, na_action='ignore').fillna(0))
        # print(df_lst['score'].sum())
        result_lst.append(df_lst['score'].sum())
    print(":: Sentiment calculated.")
    for value in result_lst:
        sentiment += int(value)
    print(":: The sum of overall sentiment is", sentiment)


if __name__ == '__main__':
    # try:
    #     opts, args = getopt.getopt(sys.argv[1:], "ho:m:a:",["help", "output="])
    # except getopt.GetoptError:
    #     print("argv error,please input")
    # for cmd, args in opts:
    #     if cmd in ("-d","--dict"):
    #         dict = arg
    file = sys.argv[1]
    dic = sys.argv[2]

    main(file, dic)
