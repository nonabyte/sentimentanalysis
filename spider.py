# -*- coding: UTF-8 -*-
from urllib import request
from bs4 import BeautifulSoup
# from selenium import webdriver


def get_page():
    url = 'https://www.imdb.com/title/tt4154796/reviews?ref_=tt_urv'

    headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'
    }

    page = request.Request(url, headers=headers)
    # Open the URL, Fetch HttpResponse and read ResponseBody
    page_info = request.urlopen(page).read().decode('utf-8')
    # Convert the content to feed BeautifulSoup，and use html.parser as parser
    soup = BeautifulSoup(page_info, 'html.parser')

    titles = soup.find_all('a', 'title')
    num = soup.find('div', 'header')
    number = str(num.div.span.text)
    number1 = int(number[0]) * 1000 + int(number[2:5])

    with open(r"./Reviews/Avengers_Endgame", "w", encoding='utf-8') as file:
        for title in titles:
            file.write(title.string)
            t = title.parent
            p = t.find('div', 'content')
            file.write(str(p.text) + '\n')
        for i in range(number1 // 25 + 1):
            loadmores = soup.find('div', 'load-more-data')
            if (loadmores == None):
                return;
            loadmorenet = "https://www.imdb.com/title/tt4154796/reviews/" + \
                          "_ajax?sort=helpfulnessScore&dir=desc&ratingFilter=0&ref_=undefined&paginationKey=" + str(
                loadmores.get('data-key'))
            page1 = request.Request(loadmorenet, headers=headers)
            page_info1 = request.urlopen(page1).read().decode('utf-8')
            # Convert the content to feed BeautifulSoup，and use html.parser as parser
            soup1 = BeautifulSoup(page_info1, 'html.parser')
            pipes = soup1.find_all('div', 'content')
            """
            opt = webdriver.ChromeOptions()
            driver = webdriver.Chrome(options=opt)
                Fill something here.
            driver.get(loadmorenet)  
            driver.find_element_by_xpath("//button[@class='ipl-load-more__button']").click()
            """
            for pipe in pipes:
                file.write(pipe.text)


get_page()
