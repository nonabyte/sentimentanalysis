### SentimentAnalysis

______

The sentiment analysis homework at UW summer school written in python3

The main aim of this project is to compare the popularity between Marvel superhero movie series *The Avengers*.



#### Project Structure

_____

- **Dictionary** folder is for the dictionary we have got in this project. Only afinn2.csv is used in current version.
- **Reviews** folder is for raw text of reviews we found on IMDB.
- **spider.py** is the spider for IMDB.
- **splitter.py** is the main function.



#### Usage

______

```
python3 spltter.py {text_path} {dictionary_path}
```



#### **Major Dependency**

______

- Pandas
- BeautifulSoup

 

